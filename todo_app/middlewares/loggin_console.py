from django.utils.deprecation import MiddlewareMixin

class AddDataRequestMiddleware(MiddlewareMixin):

    def process_request(self, request):
        full_path = request.get_full_path()
        if request.method == "POST":
            print('Action: CREATE - PATH: ', full_path)
        else:
            if 'delete' in full_path:
                print('Action: DELETE - PATH: ',full_path)
            elif 'edit' in full_path:
                print('Action: UPDATE', full_path)
            else: # caso crossoff y uncross
                method = full_path.split('/')[1]
                print('Action: UPDATE(', method ,') - PATH: ', full_path)